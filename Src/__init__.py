import shutil

COLUMNS = shutil.get_terminal_size().columns
API_KEY = "bvpe1tv48v6s82178ba0"
MENU_DICT = {
        1 : "1 FOR COMPANY SECTION",
        2 : "2 FOR STOCK LIST SECTION",
        3 : "3 FOR WATCH LIST SECTION",
        4 : "4 FOR PORTFOLIO SECTION",
        0 : "0 FOR EXIT"
    }

COMPANY_DICT = {
    1 : "1 FOR HOME",
    2 : "2 FOR COMPANY INFO",
    3 : "3 FOR COMPANY NEWS",
    0 : "0 FOR EXIT"
}

STOCK_LIST_MENU = {
    1 : "1 FOR HOME",
    2 : "2 FOR BUY",
    3 : "3 FOR SELL",
    4 : "4 FOR ADD TO WATCH LIST",
    5 : "5 FOR GET HISTORY OF STOCK",
    0 : "0 FOR EXIT"
}

WATCH_LIST_MENU = {
    1 : "1 FOR HOME",
    2 : "2 FOR REMOVE FROM WATCH LIST",
    0 : "0 FOR EXIT"
}


PORTFOLIO_MENU = {
    1 : "1 FOR HOME",
    # 2 : "2 FOR PURCHASED LIST",
    
    0 : "0 FOR EXIT"
}

code_to_name = {
    "AAPL":"Apple",
     "MSFT" : "Microsoft",
     "AMZN" : "Amazon",
     "ZM" : "Zoom",
     "TSLA" : "Tesla",
     "NFLX" : "Netflix",
     "NVDA" : "Nvidia",
     "CSCO" : "Cisco",
     "GOOGL" :  "Google",
     "FB" : "Facebook",
     "INTC" : "Intel",
     "EBAY" : "Ebay",
     "CPRT" : "Copart",
     "JD" : "JD.com",
     "ADBE" : "Adobe",
     "ADSK" : "Autodesk",
     "CTSH" : "Cognizant",
     "INTU" :"Intuit",
     "PEP" : "PepsiCo",
     "PYPL" : "PayPal"
}
stock_dict = {
    1: ("AAPL", "Apple"),
    2: ("MSFT", "Microsoft"),
    3: ("AMZN", "Amazon"),
    4: ("ZM", "Zoom"),
    5: ("TSLA", "Tesla"),
    6: ("NFLX", "Netflix"),
    7: ("NVDA", "Nvidia"),
    8: ("CSCO", "Cisco"),
    9: ("GOOGL", "Google"),
    10: ("FB", "Facebook"),
    11: ("INTC", "Intel"),
    12: ("EBAY", "Ebay"),
    13: ("CPRT", "Copart"),
    14: ("JD", "JD.com"),
    15: ("ADBE", "Adobe"),
    16: ("ADSK", "Autodesk"),
    17: ("CTSH", "Cognizant"),
    18: ("INTU", "Intuit"),
    19: ("PEP", "PepsiCo"),
    20: ("PYPL", "PayPal"),
}

def main():
    print("Initialization stage of source package")

if __name__ == "__main__":
    main()
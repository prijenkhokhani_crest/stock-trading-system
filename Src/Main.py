from __init__ import MENU_DICT,COLUMNS,COMPANY_DICT, STOCK_LIST_MENU, WATCH_LIST_MENU,PORTFOLIO_MENU
import sys
import threading
import time
from Portfolio import get_purchased_list, get_total_current_investment_value
from Company import Company
from Stock import Stock,get_stock_input,get_quantity,refresh_stock_data
def select_option(dict):
    print("\nPLEASE ENTER \n")
    for value in dict.values():
        print(value+"\n")
    
    while True:
       
        i = input()
        if(do_validation(i, len(dict))):
            if i != "0":
                key = dict.get(int(i)).split(' ', 1)[1] 
                key = key.split(' ', 1)[1]
                if key in METHOD_DICT : 
                    method1 = METHOD_DICT.get(key)
                    method1()
                return i
            else:
                print("\nTHANKS FOR VISIT US, SEE YOU SOON")
                sys.exit()
def company_menu():
    #GET_COMPANY IMFORMATION
    print("COMPANY SECTION \n".center(COLUMNS," "))
    company = Company()
    company.display_company_list()
    while True:
        j = select_option(COMPANY_DICT)
        if j == "2":
            company.get_company_profile()
        else:
            company.get_news()
    #print("kl".join(j))


def stock_list_menu():
    
    print("STOCK LIST SECTION \n".center(COLUMNS," "))
    Stock.display_stock_list()
    while True:
        j = select_option(STOCK_LIST_MENU)
        key,symbol,stock_data = get_stock_input()
        
        
        if j == "5" or j == "4":
            stock = Stock(symbol,key,stock_data.get('c'),stock_data.get('o'),stock_data.get('h'),stock_data.get('l'),stock_data.get('pc'))
            if j == "5":
                stock.display_stock_history()
            elif j == "4":
                stock.add_to_watch_list()
                print("ADD TO WATCH LIST OPERATION PERFORMED SUCCESSFULLY")
        else:
            total_item = get_quantity()
            stock = Stock(symbol,key,stock_data.get('c'),stock_data.get('o'),stock_data.get('h'),stock_data.get('l'),stock_data.get('pc'),total_item)
            if j == "2":
                stock.buy_stock()
                print("BUY OPERATION PERFORMED SUCCESSFULLY")
            elif j == "3":
                stock.sell_stock(total_item)
                print("SELL OPERATION PERFORMED SUCCESSFULLY")
            
        
    

def watch_list_menu():
    #GET_WATCH_LIST IMFORMATION
    print("WATCH LIST SECTION \n".center(COLUMNS," "))
    Stock.get_watch_list()
    while True:
        j = select_option(WATCH_LIST_MENU)
        key,symbol,stock_data = get_stock_input()
        stock = Stock(symbol,key,stock_data.get('c'),stock_data.get('o'),stock_data.get('h'),stock_data.get('l'),stock_data.get('pc'))
        if j == "2":
            stock.remove_from_watchlist()
            print("ITEM REMOVED FROM WATCH LIST SUCCESSFULLY")
        

def portfolio_menu():
    #GET_PORTFOLIO_LIST IMFORMATION
    print("PORTFOLIO SECTION \n".center(COLUMNS," "))
    while True:
        purchased_price, current_price = get_purchased_list()
        get_total_current_investment_value(purchased_price, current_price)
        select_option(PORTFOLIO_MENU)  

def refresh():
    while True:

        refresh_stock_data()
        time.sleep(500)

def main_menu():
    t = threading.Thread(target=refresh,daemon=True)
    t.start()
    select_option(MENU_DICT)

def do_validation(value, size):
    
    try:
        value = int(value)
        
        try:
            assert isinstance(value, int), "PLEASE ENTER INTEGER VALUE\n"
            assert value  in range(0,size), "PLEASE ENTER NUMBER BETWEEN 0 and {}\n".format(size)
            
            return True
        except AssertionError as a:
            print(a)
    except ValueError:
        print("PLEASE ENTER INTEGER VALUE\n")

    return False

METHOD_DICT = {
    "HOME" : main_menu,
    "COMPANY SECTION" : company_menu,
    "STOCK LIST SECTION" : stock_list_menu,
    "WATCH LIST SECTION" : watch_list_menu,
    "PORTFOLIO SECTION" : portfolio_menu
}

def main():
    welcome_string = "Welcome to STOCK TRADING SYSTEM".center(COLUMNS,"*")
    print(welcome_string)
    main_menu()
    

if __name__ == "__main__":
    main()
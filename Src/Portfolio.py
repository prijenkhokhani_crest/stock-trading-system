import os
import json
import shutil

COLUMNS = shutil.get_terminal_size().columns

CURRENT_FOLDER = os.path.dirname(__file__)
PATH_OF_STOCK = os.path.join(CURRENT_FOLDER, "stocks.json")
PATH_OF_PORTFOLIO = os.path.join(CURRENT_FOLDER, "portfolio.json")


def get_purchased_list():
    with open(PATH_OF_PORTFOLIO,"r") as file:
        port_data = json.load(file)

    with open(PATH_OF_STOCK,"r") as file:
        latest_data = json.load(file)
    
    print("{:<25} {:<25} {:<25} {:<25} {:<25} {:<25}".format('SYMBOL','NAME','PURCHASED PRICE','CURRENT PRICE', 'P/L', 'P/L(%)' ).center(COLUMNS," "))
    print("{:<25} {:<25} {:<25} {:<25} {:<25} {:<25}".format('------','-----','--------------','-------------', '----', '-----' ).center(COLUMNS," "))
   
    total_investment = 0
    current_investment = 0
    for key,value in port_data.items():
        latest_object = latest_data.get(key)
        p_price = int(value['price'])
        c_price = int(latest_object['c'])
        pl = abs(c_price - p_price)
   
              
        percentage = pl*100/p_price
        
        
        total_investment = total_investment + p_price
        
        current_investment = current_investment + c_price
        
        print("{:<25} {:<25} {:<25} {:<25} {:<25} {:<25}".format(key,value['name'],value['price'],latest_object['c'], str(pl), str(round(percentage, 2))+"%" ).center(COLUMNS," "))
    
    return total_investment, current_investment


def get_total_current_investment_value(total_investment, current_investment):
    net_pl = abs(current_investment - total_investment)
    try:
        if total_investment == 0:
            raise ZeroDivisionError('TOTAL INVESTMENT SHOULD NOT BE ZERO')
        net_pl_percentage = current_investment*100/total_investment
       
    except ZeroDivisionError as zd:
        print(zd)
    
    print('\n\n\n')
    print("{:<35} {:<35} {:<35} {:<35}".format('TOTAL INVESTMENT','CURRENT INVESTMENT','NET(P/L)', 'NET(P/L)(%)' ).center(COLUMNS," "))
    print("{:<35} {:<35} {:<35} {:<35}".format('----------------','------------------','--------', '-----------').center(COLUMNS," "))
    print("{:<35} {:<35} {:<35} {:<35}".format(total_investment,current_investment,net_pl, str(round(net_pl_percentage, 2))+"%" ).center(COLUMNS," "))

    return net_pl
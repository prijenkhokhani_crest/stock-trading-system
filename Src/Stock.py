import os
import json
import requests
from Company import Company
from Company import stock_dict as s_dict
from __init__ import code_to_name, stock_dict

import shutil

COLUMNS = shutil.get_terminal_size().columns

CURRENT_FOLDER = os.path.dirname(__file__)
PATH_OF_STOCK = os.path.join(CURRENT_FOLDER, "stocks.json")
PATH_OF_PORTFOLIO = os.path.join(CURRENT_FOLDER, "portfolio.json")
PATH_OF_WATCHLIST = os.path.join(CURRENT_FOLDER, "watchlist.json")
watchlist = set()


indexes = {"Nasdaq 100": "^NDX", "S&P 500": "^GSPC", "Dow Jones": "^DJI"}


class Stock(Company):
    def __init__(
        self,
        symbol,
        name,
        current_price,
        open_price,
        high_price_of_the_day,
        low_price_of_the_day,
        previous_close_price,
        quantity=0,
    ):
        self.symbol = symbol
        self.name = name
        self.current_price = current_price
        self.open_price = open_price
        self.high_price_of_the_day = high_price_of_the_day
        self.low_price_of_the_day = low_price_of_the_day
        self.previous_close_price = previous_close_price
        self.quantity = quantity

    def buy_stock(self):
        """
        method for Buying Stock
        """
        with open(PATH_OF_PORTFOLIO, "r") as file:
            data = json.load(file)
        if self.symbol not in data:
            portfolio = {
                self.symbol: {
                    "name": self.name,
                    "quantity": self.quantity,
                    "price": self.current_price,
                }
            }

        else:
            old_quantity = data[self.symbol]["quantity"]
            new_quantity = old_quantity + self.quantity
            new_total_price = (data[self.symbol]["price"] * old_quantity) + (
                self.current_price * self.quantity
            )
            new_total_quantity = old_quantity + self.quantity
            avg_price = new_total_price / new_quantity
            portfolio = {
                self.symbol: {
                    "name": self.name,
                    "quantity": new_total_quantity,
                    "price": avg_price,
                }
            }

        data.update(portfolio)
        with open(PATH_OF_PORTFOLIO, "w") as file:
            json.dump(data, file, indent=4)

    def sell_stock(self, quantity):
        """method to sell Stocks

        Args:
            quantity (int): No of stocks you want to sell
        """
        with open(PATH_OF_PORTFOLIO, "r") as file:
            data = json.load(file)
        if self.symbol in data:
            price =  data[self.symbol]["price"]
            available_quantity = data[self.symbol]["quantity"]
        else:
            available_quantity = 0
            print("This stocks are not available in your portfolio")
        if available_quantity < quantity:
            print("Available Quantity is less than no of shares you want to sell !")
        elif available_quantity > quantity:
            new_quantity = available_quantity - quantity
            portfolio = {
                self.symbol: {
                    "name": self.name,
                    "quantity": new_quantity,
                    "price": price,
                }
            }
            data.update(portfolio)
        else:
            del data[self.symbol]

        with open(PATH_OF_PORTFOLIO, "w") as file:
            json.dump(data, file, indent=4)

    def display_stock_history(self):
        """
        method to Display All the Stock Data in Details
        """
        with open(PATH_OF_STOCK, "r") as file:
            data = json.load(file)
        stock_data = data[self.symbol]
        print("{:<20} {:<20} {:<20} {:<20} {:<20} {:<20} {:<20}".format('COMPANY','SYMBOL','PRICE', 'HIGHEST','LOWEST','OPENING','CLOSED' ).center(COLUMNS," "))
        print("{:<20} {:<20} {:<20} {:<20} {:<20} {:<20} {:<20}".format('-------','------','------', '-------','-------','------','-------' ).center(COLUMNS," "))
        print("{:<20} {:<20} {:<20} {:<20} {:<20} {:<20} {:<20}".format(code_to_name[self.symbol],self.symbol,stock_data["c"],stock_data["h"],stock_data["l"],stock_data["o"],stock_data["pc"]).center(COLUMNS," "))
        # print("Company_Name", code_to_name[self.symbol])
        # print("Stock Symbol", self.symbol)
        # print("Stock Price ", stock_data["c"])
        # print("Today' High", stock_data["h"])
        # print("Today' Low", stock_data["l"])
        # print("Today's Opening", stock_data["o"])
        # print("Previous Closed", stock_data["pc"])

    def add_to_watch_list(self):
        """
        Adding Stocks to watch List
        """
        with open(PATH_OF_WATCHLIST, "r") as file:
            data = json.load(file)

        watch = {
                self.symbol: {
                    "name": self.name,
                    "quantity": self.quantity,
                    "price": self.current_price,
                }
            }

        watchlist.add(self.symbol)
        data.update(watch)
        with open(PATH_OF_WATCHLIST, "w") as file:
            json.dump(data,file, indent=4)
        

    def remove_from_watchlist(self):
        """
        Removing Stocks from Watch list
        """
        
        
        with open(PATH_OF_WATCHLIST, "r") as file:
            data = json.load(file)

        # watchlist.add(self.symbol)
        del data[self.symbol]
        with open(PATH_OF_WATCHLIST, "w") as file:
            json.dump(data,file, indent=4)
        

    @staticmethod
    def get_watch_list():
        """
        Watchlist for 
        """
        with open(PATH_OF_WATCHLIST, "r") as file:
            data = json.load(file)
        if not data:
            print("THERE ARE NO STOCK PRESENT IN WATCH LIST!")
           
        else:
            print("{:<35} {:<35} {:<35} ".format('SYMBOL','NAME','PRICE', ).center(COLUMNS," "))
            print("{:<35} {:<35} {:<35} ".format('------','-----','-----',).center(COLUMNS," "))
                
            print()
            for stock, detail in data.items():
                print("{:<35} {:<35} {:<35}".format(stock,detail["name"],detail["price"]).center(COLUMNS," "))
                    
                print()



    @staticmethod
    def display_stock_list():
        """
        Method for Diplaying details of Stocks
        """
        with open(PATH_OF_STOCK, "r") as file:
            data = json.load(file)
        print("{:<25} {:<25} {:<25} {:<25} {:<25}".format('SYMBOL','NAME','PRICE', 'HIGHEST', 'LOWEST').center(COLUMNS," "))
        print("{:<25} {:<25} {:<25} {:<25} {:<25}".format('------','-----','-----', '-------', '-------').center(COLUMNS," "))
        
        print()
        for stock_symbol, detail in data.items():
            print("{:<25} {:<25} {:<25} {:<25} {:<25}".format(stock_symbol,code_to_name[stock_symbol],detail["c"], detail["l"],detail["h"]).center(COLUMNS," "))
            
            print()


def get_stock_input():
    with open(PATH_OF_STOCK, "r") as file:
        data = json.load(file)
    while True:
            print("PLEASE ENTER NAME OF STOCK FROM ABOVE STOCK LIST ONLY\n")
            name = input()
            try:
                
                if name in s_dict:
                    symbol = s_dict.get(name)
                    
                    stock_data = data.get(symbol)
                    
                    return name,symbol,stock_data
                else:
                    raise ValueError("INVALID STOCK NAME! STOCK NAME DOES NOT FOUND IN ABOVE LIST\n")
            except ValueError as v:
                print(v)
def get_quantity():
    print("\n PLEASE ENTER QUANTITY OF STOCK")
    while True:
        try:
            value = int(input())
            
            return value
            
        except ValueError:
            print("PLEASE ENTER INTEGER VALUE\n")

    
def refresh_stock_data():
    """
    Refreshing stocks data
    """
    stock_data = {}
    for key, info in stock_dict.items():
        symbol, name = info
        r = requests.get(
            f"https://finnhub.io/api/v1/quote?symbol={symbol}&token=bvpbmln48v6s82175200"
        )
        stock_data[symbol] = r.json()
    #print(stock_data)
    with open(PATH_OF_STOCK, "w") as f:
        json.dump(stock_data, f, indent=4)


"""
This last few lines are for testing purpose
"""
# obj = Stock("AAPL", "Apple", 20, 225, 227, 215, 200, 5)
# ob=Stock("APPL", "Apple", 30, 225, 227, 215, 200, 5)
# obj2  Stock("MSFT","Microsoft", 220, 225, 227, 215, 200, 5)
# ob.sell_stock(10)
# obj.buy_stock()
# obj2.add_to_watch_list()
# Stock.get_watch_list()
# obj2.remove_from_watchlist()
# Stock.get_watch_list()
#Stock.display_stock_list()
# obj.display_stock_history()

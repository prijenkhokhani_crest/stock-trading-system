import requests
import shutil

COLUMNS = shutil.get_terminal_size().columns
stock_dict = {
    "Apple"     : "AAPL", 
    "Microsoft" : "MSFT", 
   "Amazon"     : "AMZN", 
    "Zoom" :  "ZM",
    "Tesla" : "TSLA", 
    "Netflix" : "NFLX", 
    "Nvidia" : "NVDA", 
    "Cisco" : "CSCO", 
    "Google" : "GOOGL", 
    "Facebook" : "FB", 
    "Intel" : "INTC", 
    "Ebay" : "EBAY", 
    "Copart" : "CPRT", 
    "JD.com" : "JD",
    "Adobe" : "ADBE",
    "Autodesk" : "ADSK", 
    "Cognizant" : "CTSH", 
    "Intuit" : "INTU", 
    "PepsiCo" : "PEP", 
    "PayPal" : "PYPL", 
}
class Company:

    def get_news(self):
        print("LATEST NEWS".center(COLUMNS," "))
        print("-----------".center(COLUMNS," "))
        while True:
            print("PLEASE ENTER COMPANY NAME FROM ABOVE COMPANY LIST ONLY OR ENTER 0 FOR GENERAL NEWS\n")
            name = input()
            if name != "0":
                try:

                    if name in stock_dict:
                        symbol = stock_dict.get(name)
                        
                        r = requests.get('https://finnhub.io/api/v1/company-news?symbol={0}&from=2020-04-30&to=2020-05-01&token=bvpe1tv48v6s82178ba0'.format(symbol))
                        r_list = r.json()
                        i =0
                        for response in r_list:
                            i = i+1
                            if i > 5:
                                break
                            print("\n".center(COLUMNS,"*"))
                            for k, v in response.items():
                                
                                if(k != "image" and k != "related" and k != "id" and k != "datetime"):
                                    label = v
                                    print("{:<15} : {}".format(k, label))
                        break
                    else:
                        raise ValueError("COMPANY NAME DOES NOT FOUND IN ABOVE LIST\n")
                except ValueError as v:
                    print(v)

            else:
                
                r = requests.get('https://finnhub.io/api/v1/news?category=general&token=bvpe1tv48v6s82178ba0')
                r_list = r.json()
                i = 0
                for response in r_list:
                    i = i+1
                    if i > 5:
                        break
                    print("\n".center(COLUMNS,"*"))
                    for k, v in response.items():
                        
                        if(k != "image" and k != "related" and k != "id" and k != "datetime"):
                            label = v
                            print("{:<15} : {}".format(k, label))
                break
        

    def display_company_list(self):
        print("FOLLOWING BELOW ARE COMPANY LIST WITH IT'S SYMBOL")
        print("{:<15} {:<45} {:<35}".format('NO. ','COMPANY NAME','SYMBOL').center(COLUMNS," "))
        i = 0
        print("{:<15} {:<45} {:<35}".format('---','------------','------').center(COLUMNS," "))
        for key, info in stock_dict.items():
            i = i+1
            print("{:<15} {:<45} {:<35}".format(i,key,info).center(COLUMNS," "))
            
    def get_company_profile(self):
        
        while True:
            print("PLEASE ENTER COMPANY NAME FROM ABOVE COMPANY LIST ONLY\n")
            name = input()
            try:

                if name in stock_dict:
                    symbol = stock_dict.get(name)
                    
                    r = requests.get('https://finnhub.io/api/v1/stock/profile2?symbol={}&token=bvpe1tv48v6s82178ba0'.format(symbol))
                    response = r.json()
                    for k, v in response.items():
                        if k != "logo":
                            label = v
                            print("{:<55} {:<50}".format(k, label).center(COLUMNS," "))
                    
                    return "Success"
                else:
                    raise ValueError("COMPANY NAME DOES NOT FOUND IN ABOVE LIST\n")
            except ValueError as v:
                print(v)

        
def main():
    print("This is the company class")

if __name__ == "__main__":
    main()
import time
import requests
import json
import os
from Company import stock_dict
CURRENT_FOLDER = os.path.dirname(__file__)
PATH_OF_STOCK = os.path.join(CURRENT_FOLDER, "stocks.json")
while True:
    time.sleep(10)
    def refresh_stock_data():
        """
        Refreshing stocks data
        """
        stock_data = {}
        for key, info in stock_dict.items():
            symbol, name = info
            r = requests.get(
                f"https://finnhub.io/api/v1/quote?symbol={symbol}&token=bvpbmln48v6s82175200"
            )
            stock_data[symbol] = r.json()
        print(stock_data)
        with open(PATH_OF_STOCK, "w") as f:
            json.dump(stock_data, f, indent=4)

